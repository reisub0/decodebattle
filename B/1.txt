You are given a number lock which has m numbers. You know the combination which will unlock the lock. Given the initial combination, find the minimum number of moves needed to unlock the lock. In one move, one of the numbers can be incremented by 1 or decremented by 1. Note that increment/decrement happens cyclically (i.e. 9+1 = 0 and 0-1 = 9).

Input

First line contains one integer m - the number of numbers in the lock.
Second line contains m space separated integers - the initial state of the lock.
Third line contains m space separated integers - the final(unlocking) state of the lock.

Output

Print one integer - the minimum number of moves necessary to unlock the lock.

Constraints

1 ≤ m ≤ 10^5
All numbers on the lock are between 0 and 9. (inclusive)

Example 1

4
1 2 3 4
5 6 7 8

Answer: 16
